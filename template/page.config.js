/**
 * Created by zhangsir on 2018/6/13.
 */
module.exports = {
  index: {
    js: './src/pages/index/index.js',
    html: './src/pages/index/index.html'
  },
  page2: {
    js: './src/pages/page2/page2.js',
    html: './src/pages/page2/page2.html'
  }
}
