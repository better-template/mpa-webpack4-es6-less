let webpack = require('webpack');
const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const CleanWebpackPlugin = require('clean-webpack-plugin');
let pageConf = require('./page.config');
let isProdution = process.env.NODE_ENV !== 'dev';
let entry = {}

Object.keys(pageConf).forEach(item => {
  entry[item] = pageConf[item].js
})

module.exports = {
  mode: isProdution ? 'production' : 'development',
  // 配置入口
  entry: entry,
  // 配置出口
  output: {
    path: __dirname + "/dist/",
    filename: 'js/[name]-[hash:5].js',
    publicPath: isProdution ? './' : '/',
  },

  module: {
    rules: [
      //解析.js
      {
        test: '/\.js$/',
        loader: 'babel',
        exclude: path.resolve(__dirname, 'node_modules'),
        include: path.resolve(__dirname, 'src'),
        query: {
          presets: ['env']
        }
      },
      // css处理
      {
        test: /\.css$/,
        loader: 'style-loader!css-loader'

      },
      // less处理
      {
        test: /\.less$/,
        loader: 'style-loader!css-loader!less-loader'
      },
      // 图片处理
      {
        test: /\.(png|jpg|gif|svg)$/,
        loader: 'file-loader',

        query: {
          name: 'assets/[name]-[hash:5].[ext]'
        },
      }, {
        test: /\.(htm|html)$/i,
        use: ['html-withimg-loader']
      }
    ]
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new ExtractTextPlugin(__dirname + '/assert/css/common.less'),
    new CleanWebpackPlugin(
      ['dist/*', 'dist/*',],　     //匹配删除的文件
      {
        root: __dirname,       　　　　　　　　　　//根目录
        verbose: true,        　　　　　　　　　　//开启在控制台输出信息
        dry: false        　　　　　　　　　　//启用删除文件
      }
    )
  ].concat(Object.keys(pageConf).map(item => {
    let conf = pageConf[item] || {}
    let html = conf.html || ''
    let arr = html.split('/')
    let template = arr[arr.length - 1]

    return isProdution ? new HtmlWebpackPlugin({
      filename: __dirname + '/dist/' + template,
      template: path.join(__dirname, html),
      chunks: [template.replace('.html', '')],
      inlineSource: '.(js|css)$',
      minify: {
        removeComments: true,//删除注释
        collapseWhitespace: true//删除空格
      }
    }) : new HtmlWebpackPlugin({
      filename: __dirname + '/dist/' + template,
      template: path.join(__dirname, html),
      chunks: [template.replace('.html', '')],
      inlineSource: '.(js|css)$'
    })
  })),
  // 起本地服务，我起的dist目录
  devServer: {
    contentBase: "./dist/",
    historyApiFallback: true,
    inline: true,
    open: true,
    hot: true,
    progress: true,
    host: '0.0.0.0',//我的局域网ip
  }
}
